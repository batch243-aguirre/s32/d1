//[CREATE A SERVER]

//1. import http through "require" directive
//2. we use the createServer() method
//3. Define the port number that the server you will be listening to
//4. use the listen() method for the server to run in a specified port
//5. use the writeHead method to set server status & content-type of server message
//6. use the end() method to set the server message
//7. console log the to monitor the server status


//GOAL FOR DISCUSSION: Create a condition and response when the route "/items" is accessed "

let http = require("http");
const port= 4000


http.createServer(function(request,response){
	

//HTTP Routing Methods: Get, Post, Put, Delete 
	if(request.url == "/items" && request.method == "GET" ){

		//HTTP method of the incoming request can be accessed via the method propert of the request parameter
		//The method "GET" means that we will be retrieving or reading an information 
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Data retrieved from the database")
	}

	else if(request.url == "/items" && request.method == "POST"){

		//Requests the '/items' path and "SENDS" information
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("Data to be sent to the database")
	}

//NOTE: You may either use a variable to set a port or hard code the port number that you want to connect to
}).listen(port);

console.log('Server running at localhost:4000');
